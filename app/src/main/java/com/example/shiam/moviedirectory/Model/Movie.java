package com.example.shiam.moviedirectory.Model;

public class Movie {
    private String poster;
    private String title;
    private String movieType;
    private String year;
    private String imdbId;


    public Movie(String poster, String title, String movieType, String year, String imdbId) {
        this.poster = poster;
        this.title = title;
        this.movieType = movieType;
        this.year = year;
        this.imdbId = imdbId;
    }

    public Movie(){

    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMovieType() {
        return movieType;
    }

    public void setMovieType(String movieType) {
        this.movieType = movieType;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String category) {
        this.year = category;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }
}
